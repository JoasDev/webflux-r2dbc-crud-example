package io.gitlab.joasdev.crud.example.persistence.dao;

import io.gitlab.joasdev.crud.example.model.dto.AuthorCriteria;
import io.gitlab.joasdev.crud.example.persistence.entity.Author;
import reactor.core.publisher.Flux;

public interface AuthorDao {
	Flux<Author> findAuthorsByCriteria(AuthorCriteria authorCriteria);
}
