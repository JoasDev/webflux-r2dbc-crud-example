package io.gitlab.joasdev.crud.example.persistence.entity;

import java.io.Serializable;
import java.time.LocalDate;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Table(name = "books")
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class Book implements Serializable {

    private static final long serialVersionUID = 2426757912628209071L;

    @Id
    @Column("book_id")
    private Integer bookId;
    @Column("title")
    private String title;
    
    @JsonFormat(pattern = "dd/MM/yyyy")
    @Column("publication_date")
    private LocalDate publicationDate;
    @Column("online_availability")
    private Boolean onlineAvailability;
    
    //@Transient
    //private String concatAuthors;
}
