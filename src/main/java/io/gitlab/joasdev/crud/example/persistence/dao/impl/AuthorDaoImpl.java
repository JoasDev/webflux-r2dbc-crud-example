package io.gitlab.joasdev.crud.example.persistence.dao.impl;

import org.springframework.data.r2dbc.core.R2dbcEntityTemplate;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.data.relational.core.query.Query;
import org.springframework.stereotype.Repository;

import io.gitlab.joasdev.crud.example.model.dto.AuthorCriteria;
import io.gitlab.joasdev.crud.example.persistence.dao.AuthorDao;
import io.gitlab.joasdev.crud.example.persistence.entity.Author;
import lombok.AllArgsConstructor;
import reactor.core.publisher.Flux;

@AllArgsConstructor
@Repository
public class AuthorDaoImpl implements AuthorDao{
	private R2dbcEntityTemplate r2dbcEntityTemplate;
	
	@Override
	public Flux<Author> findAuthorsByCriteria(AuthorCriteria bookCriteria) {
		Criteria criteria = Criteria.empty();

        if (bookCriteria.getAuthorName() != null) {
            criteria = criteria.and(Criteria.where("authorName").like("%"+bookCriteria.getAuthorName()+"%"));
        }

        if (bookCriteria.getLastName() != null) {
            criteria = criteria.and(Criteria.where("lastName").like("%"+bookCriteria.getLastName()+"%"));
        }
        Query query = Query.query(criteria);
     
        
        return r2dbcEntityTemplate
                .select(Author.class)
                .matching(query)                
                .all();
   }
	
}
