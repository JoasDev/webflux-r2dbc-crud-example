package io.gitlab.joasdev.crud.example.persistence.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Modifying;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

import io.gitlab.joasdev.crud.example.model.projection.AuthorProjection;
import io.gitlab.joasdev.crud.example.persistence.dao.AuthorDao;
import io.gitlab.joasdev.crud.example.persistence.entity.Author;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface AuthorRepository extends ReactiveCrudRepository<Author, Integer>,AuthorDao {
	
	@Modifying
    @Query("INSERT INTO authors (first_name,last_name,birthdate) VALUES "
            + "(:#{#author.firstName}, :#{#author.lastName}, :#{#author.birthdate})")
    Mono<Integer> saveAuthor(@Param("author") Author author);

    
    @Query("UPDATE authors SET first_name=:#{#author.firstName}, last_name=:#{#author.lastName}, birthdate=:#{#author.birthdate} "
            + "WHERE author_id = :#{#author.authorId}")
    Mono<Integer> update(@Param("author") Author author);

       
    @Query("SELECT a.author_id, a.first_name, a.last_name, "
            + "CONCAT(a.first_name,' ',a.last_name) as full_name, a.birthdate "
            + "FROM authors a WHERE a.author_id = :authorId")
    Mono<AuthorProjection> findByUserId(@Param("authorId") Integer authorId);

    
    @Query("SELECT a.author_id, a.first_name, a.last_name, a.first_name || ' ' || a.last_name as full_name, a.birthdate "
            + "FROM authors a WHERE a.first_name like CONCAT('%',:q ,'%') or a.last_name like CONCAT('%',:q ,'%') "
            + "ORDER BY a.author_id asc "
            + "LIMIT :#{#pageable.pageSize} OFFSET :#{#pageable.offset}")
	Flux<AuthorProjection> findByQ(@Param("q") String q, @Param("pageable") Pageable pageable);
    
    @Query("SELECT count(a.author_id) "
            + "FROM authors a WHERE a.first_name like '%' || :q || '%' or a.last_name like '%' || :q || '%'")
	Mono<Integer> findCountByQ(@Param("q") String q);
    
}
