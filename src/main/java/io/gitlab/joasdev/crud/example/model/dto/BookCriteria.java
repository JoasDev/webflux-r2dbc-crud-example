package io.gitlab.joasdev.crud.example.model.dto;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class BookCriteria {
	private String q;    
    private LocalDate publicationDate;
}
