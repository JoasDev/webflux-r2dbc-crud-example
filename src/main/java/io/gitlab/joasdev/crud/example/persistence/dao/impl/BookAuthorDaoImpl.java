package io.gitlab.joasdev.crud.example.persistence.dao.impl;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.r2dbc.core.DatabaseClient;
import org.springframework.r2dbc.core.DatabaseClient.GenericExecuteSpec;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import io.gitlab.joasdev.crud.example.exception.ApiException;
import io.gitlab.joasdev.crud.example.model.dto.BookCriteria;
import io.gitlab.joasdev.crud.example.model.projection.BookProjection;
import io.gitlab.joasdev.crud.example.model.vo.BookVo;
import io.gitlab.joasdev.crud.example.persistence.dao.BookAuthorDao;
import io.gitlab.joasdev.crud.example.persistence.entity.BookAuthor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Slf4j
@RequiredArgsConstructor
@Repository
public class BookAuthorDaoImpl implements BookAuthorDao {
	//private final R2dbcEntityTemplate r2dbcEntityTemplate;
	private final DatabaseClient databaseClient;
	
	@Override
	public Mono<Void> saveBookAuthor(BookAuthor bookAuthor) {

		return databaseClient.sql("INSERT INTO book_authors (book_id, author_id) VALUES (:bookId, :authorId)")
	            .bind("bookId", bookAuthor.getBookId())
	            .bind("authorId", bookAuthor.getAuthorId())
	            .fetch()
	            .rowsUpdated()
	            .then()
	            .onErrorResume(error -> {
		            return Mono.error(new ApiException(error.getMessage(),HttpStatus.NOT_FOUND));
		        });
	}

	@Override
	public Flux<Void> saveAllBookAuthor(List<BookAuthor> bookAuthors) {
				
        List<Mono<Void>> inserts = bookAuthors.stream()
        .map(bookAuthor -> 
	        databaseClient.sql("INSERT INTO book_authors (book_id, author_id) VALUES (:bookId, :authorId)")
	        .bind("bookId", bookAuthor.getBookId())
	        .bind("authorId", bookAuthor.getAuthorId())
	        .fetch()
	        .rowsUpdated()
	        .then()
	        .onErrorMap(e -> new ApiException(e.getMessage(),HttpStatus.NOT_FOUND))
        )
        .collect(Collectors.toList());
        
        
        return Flux.concat(inserts);
		
	}

	@Override
	public Mono<BookProjection> findByBookId(Integer bookId) {
		
		
		String sql  = """				
				SELECT ba.book_id as bookId, b.title as title, b.publication_date as publicationDate, b.online_availability as onlineAvailability,  
				GROUP_CONCAT(a.first_name||' '||a.last_name SEPARATOR ', ') as concatAuthors 
                FROM book_authors ba 
                INNER JOIN books b ON ba.book_id = b.book_id 
                INNER JOIN authors a ON ba.author_id = a.author_id 
                WHERE b.book_id = :bookId GROUP BY b.book_id
				""";
		
		return databaseClient.sql(sql)
	            .bind("bookId", bookId)
	            .map((row, metadata) -> {
	            	log.info("publicationDate {} ", metadata.getColumnMetadata("publicationDate"));
	                BookProjection bookProjection = BookVo.builder()
	                		.bookId(row.get("bookId",Integer.class))
	                		.title(row.get("title",String.class))
	                		//.publicationDate(row.get("publicationDate",LocalDateTime.class) != null ? row.get("publicationDate",LocalDateTime.class).toLocalDate() : null)
	                		.publicationDate(row.get("publicationDate", LocalDate.class))
	                		.onlineAvailability(row.get("onlineAvailability",Boolean.class))
	                		.concatAuthors(row.get("concatAuthors",String.class))
	                		.build();
	                
	                return bookProjection;
	            }).first()
	            .switchIfEmpty(Mono.error(new ApiException("No record found for book with ID: " + bookId, HttpStatus.NOT_FOUND)));
	}
	@Override
	public Flux<BookProjection> findAllToPage(BookCriteria bookCriteria, Pageable pageable) {

		String select  = """				
				SELECT b.book_id as bookId, b.title as title, b.publication_date as publicationDate, b.online_availability as onlineAvailability,  
				GROUP_CONCAT(a.first_name||' '||a.last_name SEPARATOR ', ') as concatAuthors 
				""";
		
		String from = """		
                FROM book_authors ba 
                INNER JOIN books b ON ba.book_id = b.book_id 
                INNER JOIN authors a ON ba.author_id = a.author_id 
                """;
		
		String where = "";
		
		StringBuilder sqlWhere = new StringBuilder();
        boolean flag = false;
        if(StringUtils.hasText(bookCriteria.getQ())) {
            
            if(flag) {
                sqlWhere.append("OR ");
            }
            
            sqlWhere.append("b.title LIKE :q ");
            flag = true;
        }
        
        if(bookCriteria.getPublicationDate()!=null) {
            
            if(flag) {
                sqlWhere.append("OR ");
            }
            
            sqlWhere.append("b.publicationDate = :publicationDate ");
            flag = true;
        }
        
        if(flag) {
        	where = sqlWhere.insert(0, "WHERE ").toString();
        }
		
        String limit = """
        		GROUP BY b.book_id 
                ORDER BY b.book_id asc 
                LIMIT :pageSize OFFSET :offset
				""";
		
		String sql = select+from+where+limit;
		log.info(sql);
		GenericExecuteSpec ges = databaseClient.sql(sql);
		
		if(StringUtils.hasText(bookCriteria.getQ())) {
			ges = ges.bind("q", "%"+bookCriteria.getQ()+"%");
        }
		
		if(bookCriteria.getPublicationDate()!=null) {
			ges = ges.bind("publicationDate", bookCriteria.getPublicationDate());
        }
		
		return ges.bind("pageSize", pageable.getPageSize())
				.bind("offset",pageable.getOffset() )
	            .map((row, metadata) -> {
	            	log.info("publicationDate {} ", metadata.getColumnMetadata("publicationDate"));
	            	log.info("bookId {} ", metadata.getColumnMetadata("bookId").toString());
	                BookProjection bookProjection = BookVo.builder()
	                		.bookId(row.get("bookId",Integer.class))
	                		.title(row.get("title",String.class))
	                		.publicationDate(row.get("publicationDate", LocalDate.class))
	                		.onlineAvailability(row.get("onlineAvailability",Boolean.class))
	                		.concatAuthors(row.get("concatAuthors",String.class))
	                		.build();
	                
	                return bookProjection;
	            }).all(); 
		
		/*
		return databaseClient.sql(sql)
	            .bind("q", bookCriteria.getQ())
	            .bind("pageSize", pageable.getPageSize())
	            .bind("offset",pageable.getOffset() )
	            .map((row, metadata) -> {
	            	log.info("publicationDate {} ", metadata.getColumnMetadata("publicationDate"));
	                BookProjection bookProjection = BookVo.builder()
	                		.bookId(row.get("bookId",Integer.class))
	                		.title(row.get("title",String.class))
	                		.publicationDate(row.get("publicationDate", LocalDate.class))
	                		.onlineAvailability(row.get("onlineAvailability",Boolean.class))
	                		.concatAuthors(row.get("concatAuthors",String.class))
	                		.build();
	                
	                return bookProjection;
	            }).all();*/
		
	}

	@Override
	public Flux<BookProjection> findAllBookAuthorByBookId(Integer bookId) {
		
		String sql  = """				
				SELECT ba.book_id as bookId, b.title as title, b.publication_date as publicationDate, b.online_availability as onlineAvailability,  
				GROUP_CONCAT(a.first_name||' '||a.last_name SEPARATOR ', ') as concatAuthors 
                FROM book_authors ba 
                INNER JOIN books b ON ba.book_id = b.book_id 
                INNER JOIN authors a ON ba.author_id = a.author_id 
                WHERE b.book_id = :bookId GROUP BY b.book_id
				""";
		
		
		return databaseClient.sql(sql)
	            .bind("bookId", bookId)
	            .map((row, metadata) -> {
	            	log.info("publicationDate {} ", metadata.getColumnMetadata("publicationDate"));
	                BookProjection bookProjection = BookVo.builder()
	                		.bookId(row.get("bookId",Integer.class))
	                		.title(row.get("title",String.class))
	                		.publicationDate(row.get("publicationDate", LocalDate.class))
	                		.onlineAvailability(row.get("onlineAvailability",Boolean.class))
	                		.concatAuthors(row.get("concatAuthors",String.class))
	                		.build();
	                
	                return bookProjection;
	            }).all()
	            .switchIfEmpty(Mono.error(new ApiException("No record found.", HttpStatus.NOT_FOUND)));
	}

	@Override
	public Mono<Boolean> existBookAuthorByBookId(Integer bookId) {

		String sql  = """				
				SELECT case when count(ba.book_id) > 0 then true else false end as result  
                FROM book_authors ba 
                WHERE ba.book_id = :bookId
				""";
		
		return databaseClient.sql(sql)
	            .bind("bookId", bookId)
	            .map((row, metadata) -> {
	                return row.get("result",Boolean.class);
	            }).first()
	            .switchIfEmpty(Mono.error(new ApiException("No record found for book with ID: " + bookId, HttpStatus.NOT_FOUND)));
	
	}
	
	@Override
	public Mono<Boolean> existBookAuthorByAuthorId(Integer author_id) {

		String sql  = """				
				SELECT case when count(ba.book_id) > 0 then true else false end as result  
                FROM book_authors ba 
                WHERE ba.author_id = :author_id
				""";
		
		return databaseClient.sql(sql)
	            .bind("author_id", author_id)
	            .map((row, metadata) -> {
	                return row.get("result",Boolean.class);
	            }).first()
	            .switchIfEmpty(Mono.error(new ApiException("No record found for author with ID: " + author_id, HttpStatus.NOT_FOUND)));
	
	}

	@Override
	public Mono<Void> deleteBookAuthorByBookId(Integer bookId) {

		String sql  = """			
                DELETE FROM book_authors ba 
                WHERE ba.book_id = :bookId
				""";
		
		return databaseClient.sql(sql)
	            .bind("bookId", bookId)
	            .fetch()
	            .rowsUpdated()
	            .then()
	            .onErrorMap(t -> {
	            	log.error(t.getMessage());
	            	return new ApiException("Error in delete book_authors, bookId "+bookId, HttpStatus.NOT_FOUND);
	            	
	            });
	
	}
	
	@Override
	public Mono<Void> deleteBookAuthorByAuthorId(Integer authorId) {

		String sql  = """			
                DELETE FROM book_authors ba 
                WHERE ba.authorId = :authorId
				""";
		
		return databaseClient.sql(sql)
	            .bind("authorId", authorId)
	            .fetch()
	            .rowsUpdated()
	            .then()
	            .onErrorMap(t -> {
	            	log.error(t.getMessage());
	            	return new ApiException("Error in delete book_authors, authorId "+authorId, HttpStatus.NOT_FOUND);
	            	
	            });
	
	}

	@Override
	public Mono<Long> findCountBookAuthorByCriteria(BookCriteria bookCriteria) {
		
		String select = "SELECT count(b.book_id) as result ";
		String from = "FROM books b ";
		String where = "";
		
		StringBuilder sqlWhere = new StringBuilder();
        boolean flag = false;
        if(StringUtils.hasText(bookCriteria.getQ())) {
            
            if(flag) {
                sqlWhere.append("OR ");
            }
            
            sqlWhere.append("b.title LIKE :q ");
            flag = true;
        }
        
        if(bookCriteria.getPublicationDate()!=null) {
            
            if(flag) {
                sqlWhere.append("OR ");
            }
            
            sqlWhere.append("b.publicationDate = :publicationDate ");
            flag = true;
        }
        
        if(flag) {
        	where = sqlWhere.insert(0, "WHERE ").toString();
        }
		 
		String sql = select+from+where;
		log.info(sql);
		GenericExecuteSpec ges = databaseClient.sql(sql);
		
		if(StringUtils.hasText(bookCriteria.getQ())) {
			ges = ges.bind("q","%"+ bookCriteria.getQ()+"%");
        }
		
		if(bookCriteria.getPublicationDate()!=null) {
			ges = ges.bind("publicationDate", bookCriteria.getPublicationDate());
        }
		
		return ges.map((row, metadata) -> {
					log.info("count result {}",metadata.getColumnMetadata("result").toString());
	                return row.get("result",Long.class);
	            }).first()
	            .switchIfEmpty(Mono.error(new ApiException("No record found for book", HttpStatus.NOT_FOUND)));
	
	}
	
	

}
