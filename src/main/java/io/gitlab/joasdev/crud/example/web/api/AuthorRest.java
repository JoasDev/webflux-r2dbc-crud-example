package io.gitlab.joasdev.crud.example.web.api;


import java.util.Arrays;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.gitlab.joasdev.crud.example.exception.ApiException;
import io.gitlab.joasdev.crud.example.model.dto.AuthorCriteria;
import io.gitlab.joasdev.crud.example.model.dto.AuthorFilter;
import io.gitlab.joasdev.crud.example.model.dto.RegisterAuthorDto;
import io.gitlab.joasdev.crud.example.model.dto.UpdateAuthorDto;
import io.gitlab.joasdev.crud.example.model.projection.AuthorProjection;
import io.gitlab.joasdev.crud.example.persistence.entity.Author;
import io.gitlab.joasdev.crud.example.service.AuthorService;
import io.gitlab.joasdev.crud.example.web.util.ResponseMessage;
import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping(value = "/authors")
@RequiredArgsConstructor
public class AuthorRest {
	
    private final AuthorService authorService;
    
    @PostMapping
    public Mono<ResponseEntity<Void>> registerAuthor(@RequestBody RegisterAuthorDto registerAuthorDto) throws ApiException {
        
        return authorService.saveAuthor(registerAuthorDto)
                .flatMap(authorId ->  Mono.just(new ResponseEntity<>(HttpStatus.CREATED)));
    }

    @GetMapping("/pages")
    public Mono<ResponseEntity<Page<AuthorProjection>>> findAllToPage(
            @RequestParam(name="q",defaultValue = "",required = false) String q,
            @RequestParam(name = "page", defaultValue = "0", required = false) int page,
            @RequestParam(name = "size", defaultValue = "5", required = false) int size,
            @RequestParam(name = "sortBy", defaultValue = "authorId", required = false)String sortBy,
            @RequestParam(name = "sortDirection", defaultValue = "asc", required = false) String sortDirection
    		) throws ApiException {

    	String[] sortArray = sortBy.contains(",")
    	          ? Arrays.stream(sortBy.split(",")).map(String::trim).toArray(String[]::new)
    	          : new String[] { sortBy.trim() };
    	
    	Sort sort = Sort.by(Direction.fromString(sortDirection), sortArray);
    	
        Pageable pageable = PageRequest.of(page, size, sort);

        AuthorFilter authorSpecification = AuthorFilter.builder()
                .q(q)
                .build();

        return authorService.findAllToPage(authorSpecification,pageable)
                .flatMap(authors -> Mono.just(ResponseEntity.ok(authors)));
    }
    
    @GetMapping(value="/{authorId}")
    public Mono<ResponseEntity<AuthorProjection>> getAuthor(@PathVariable("authorId") Integer authorId) throws ApiException {
        
        return  authorService.findAuthorId(authorId)                
                .flatMap( authorProjection -> Mono.just(new ResponseEntity<>(authorProjection, HttpStatus.OK)));
    }
    
    
    @PutMapping("/{authorId}")
    public Mono<ResponseEntity<ResponseMessage<AuthorProjection>>> registerAuthor(@PathVariable("authorId") Integer authorId,@RequestBody UpdateAuthorDto updateAuthorDto) throws ApiException {
        
        return authorService.updateAuthor(authorId, updateAuthorDto)
                .flatMap(authorProjection -> {
                    ResponseMessage<AuthorProjection> msg = ResponseMessage.<AuthorProjection>builder()
                            .message("Registro modificado")
                            .content(authorProjection)
                            .build();
                    
                    return Mono.just(msg);
                }).flatMap(msg -> Mono.just(new ResponseEntity<>(msg,HttpStatus.OK)));
    }
    
    @DeleteMapping("/{authorId}")
    public Mono<ResponseEntity<Void>> deleteAuthor(@PathVariable("authorId") Integer authorId) throws ApiException {
        
        return authorService.deleteAuthor(authorId)
                .then(Mono.just(new ResponseEntity<>(HttpStatus.OK)));
                //.switchIfEmpty(ServerResponse.notFound().build());
        
    }
    
    @GetMapping("/list")
    public Flux<Author> getBook(AuthorCriteria authorCriteria) {
               
        return authorService.findAll(authorCriteria);
    }

}
