package io.gitlab.joasdev.crud.example.web.api;

import java.time.LocalDate;
import java.util.Arrays;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.gitlab.joasdev.crud.example.model.dto.BookCriteria;
import io.gitlab.joasdev.crud.example.model.dto.RegisterBookDto;
import io.gitlab.joasdev.crud.example.model.projection.BookProjection;
import io.gitlab.joasdev.crud.example.service.BookService;
import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Mono;

@RequiredArgsConstructor
@RestController
@RequestMapping("/books")
public class BookRest {
    
    private final BookService bookService;

    @PostMapping
    public Mono<ResponseEntity<Void>> registerBook(@RequestBody RegisterBookDto registerBookDto) {
        
        return bookService.saveBook(registerBookDto)
                .then(Mono.just(new ResponseEntity<>(HttpStatus.CREATED)));
    }

    @GetMapping("/pages")
    public Mono<ResponseEntity<Page<BookProjection>>> findAllToPage(
            @RequestParam(name="q",defaultValue = "",required = false) String q,
            @DateTimeFormat(pattern = "dd/MM/yyyy") LocalDate publicationDate,
            @RequestParam(name = "page", defaultValue = "0", required = false) int page,
            @RequestParam(name = "size", defaultValue = "5", required = false) int size,
            @RequestParam(name = "sortBy", defaultValue = "bookId", required = false)String sortBy,
            @RequestParam(name = "sortDirection", defaultValue = "asc", required = false) String sortDirection) {
    	
    	String[] sortArray = sortBy.contains(",")
  	          ? Arrays.stream(sortBy.split(",")).map(String::trim).toArray(String[]::new)
  	          : new String[] { sortBy.trim() };
  	
    	Sort sort = Sort.by(Direction.fromString(sortDirection), sortArray);
    	
        Pageable pageable = PageRequest.of(page, size, sort);

        BookCriteria bookSpec = BookCriteria.builder()
                .q(q)
                .publicationDate(publicationDate)
                .build();
        
       return bookService.findAllToPage(bookSpec,pageable)
    		   .flatMap(books -> Mono.just(ResponseEntity.ok(books)));
    }
    
    @GetMapping("/{bookId}")
    public Mono<ResponseEntity<BookProjection>> getBook(@PathVariable("bookId") Integer bookId) {
               
        return bookService.findBookId(bookId)
                .flatMap(book -> Mono.just(new ResponseEntity<>(book, HttpStatus.OK)));
    }
    
    
    
    @DeleteMapping("/{bookId}")
    public Mono<ResponseEntity<Void>> deleteBook(@PathVariable("bookId") Integer bookId) {
           	
        return bookService.deleteBook(bookId)
                .then(Mono.just(new ResponseEntity<>(HttpStatus.OK)));
    }
}
