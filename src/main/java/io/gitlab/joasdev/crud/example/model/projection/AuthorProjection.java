package io.gitlab.joasdev.crud.example.model.projection;

import java.time.LocalDate;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

//@ProjectedPayload
@JsonPropertyOrder(value = {"authorId","firstName","lastName","fullName","birthdate"})
public interface AuthorProjection {
    Integer getAuthorId();

    String getFirstName();

    String getLastName();

    default String getFullName() {
        if (getFirstName() == null || getLastName() == null) {
            return "";
        }
        return getFirstName() + " " + getLastName();
    }

    @JsonFormat(pattern = "dd-MM-yyyy")
    LocalDate getBirthdate();
}
