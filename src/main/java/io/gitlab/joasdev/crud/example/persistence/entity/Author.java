package io.gitlab.joasdev.crud.example.persistence.entity;

import java.io.Serializable;
import java.time.LocalDate;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Table(name = "authors")
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class Author implements Serializable{

    private static final long serialVersionUID = 4792395626112349375L;
    
    @Id
    @Column("author_id")
    private Integer authorId;
    @Column("first_name")
    private String firstName;
    @Column("last_name")
    private String lastName;
    
    @JsonFormat(pattern = "dd/MM/yyyy")
    @Column("birthdate")
    private LocalDate birthdate;
    
}
