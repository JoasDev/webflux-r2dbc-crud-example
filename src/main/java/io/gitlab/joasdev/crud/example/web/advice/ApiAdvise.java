package io.gitlab.joasdev.crud.example.web.advice;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import io.gitlab.joasdev.crud.example.exception.ApiException;
import io.gitlab.joasdev.crud.example.web.util.ResponseMessage;
import reactor.core.publisher.Mono;

@RestControllerAdvice
public class ApiAdvise {

    @ExceptionHandler(value = ApiException.class)
    public Mono<ResponseEntity<ResponseMessage<Void>>> getException(ApiException exception) {

        ResponseMessage<Void> msg = ResponseMessage.<Void>builder()
                .message(exception.getMessage())
                .build();
        
        return Mono.just(new ResponseEntity<>(msg,exception.getHttpStatus()));
    }
    
 

}
