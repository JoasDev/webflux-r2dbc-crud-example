# WebFlux + R2DBC Crud Example
This project gives examples of how to do a CRUD with Spring WebFlux + R2DBC

[![Construye un CRUD de Alto Rendimiento 🔥🔥🔥](https://i.ytimg.com/vi/s6qKE0FD3BU/maxresdefault.jpg)](https://www.youtube.com/live/s6qKE0FD3BU)

[https://www.youtube.com/live/s6qKE0FD3BU](https://www.youtube.com/live/s6qKE0FD3BU)

## Requirements
- [Java 17](https://adoptium.net/) or higher. Setting the [JAVA_HOME](https://www.baeldung.com/java-home-on-windows-7-8-10-mac-os-x-linux) environment variable
- [Maven](https://maven.apache.org/download.cgi)
- [Lombok](https://projectlombok.org/download)

## Project features

- [Spring WebFlux](https://docs.spring.io/spring-framework/reference/web/webflux.html)
- [R2DBC](https://r2dbc.io/)
- [Cors](https://www.baeldung.com/spring-cors)
- [ModelMapper](https://modelmapper.org/)
- [Lombok](https://projectlombok.org/)
- [ReactiveCrudRepository](https://spring.io/guides/gs/accessing-data-r2dbc/)
- DAO
- Create your own insert, update, and delete queries
- [Projection](https://www.baeldung.com/spring-data-jpa-projections)
- [R2dbcEntityTemplate](https://docs.spring.io/spring-data/r2dbc/docs/current-SNAPSHOT/reference/html/#r2dbc.core)
- [DatabaseClient](https://hantsy.github.io/spring-r2dbc-sample/database-client.html)
- [Pagination](https://prateek-ashtikar512.medium.com/r2dbc-pagination-example-1450a5dbdce8)
- Criteria
- [RestControllerAdvice](https://www.baeldung.com/spring-boot-custom-webflux-exceptions)
- Generics
- Transactional
- H2

## Instructions

If you want to run the project and test from consoleIf you want to run the project and test from console

```
mvn clean install
mvn spring-boot:run
```

You can test the API through a tool to make HTTP requests like [Insomnia](https://insomnia.rest/download).
```
doc/Insomnia_Crud_example.json

```

## Social networks
*  [Discord](https://discord.gg/VdEw8UKx5z)
*  [Twitch](https://www.twitch.tv/tetradotoxina)
*  [Youtube Oficial](https://www.youtube.com/@TetradotoxinaOficial)
*  [Youtube Developers](https://www.youtube.com/@JoasDev)
*  [Fanpage Oficial (Entertainment)](https://bit.ly/3wHqavn)
*  [Fanpage Developers (Developers)](https://bit.ly/3upIG9V)
*  [Facebook Group (Developers)](https://bit.ly/3bTZaAQ)
*  [Web](https://tetradotoxina.com)
