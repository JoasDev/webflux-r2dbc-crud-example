package io.gitlab.joasdev.crud.example.persistence.dao;

import java.util.List;

import org.springframework.data.domain.Pageable;

import io.gitlab.joasdev.crud.example.model.dto.BookCriteria;
import io.gitlab.joasdev.crud.example.model.projection.BookProjection;
import io.gitlab.joasdev.crud.example.persistence.entity.BookAuthor;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface BookAuthorDao {
	Mono<Long> findCountBookAuthorByCriteria(BookCriteria bookCriteria);
	Mono<Void> saveBookAuthor(BookAuthor bookAuthor);
	Flux<Void> saveAllBookAuthor(List<BookAuthor> bookAuthor);
	Mono<BookProjection> findByBookId(Integer bookId);
	Mono<Boolean> existBookAuthorByBookId(Integer bookId);
	Mono<Boolean> existBookAuthorByAuthorId(Integer authorId);
	Flux<BookProjection> findAllBookAuthorByBookId(Integer bookId);
	Mono<Void> deleteBookAuthorByBookId(Integer bookId);
	Mono<Void> deleteBookAuthorByAuthorId(Integer authorId);
	Flux<BookProjection> findAllToPage(BookCriteria bookCriteria, Pageable pageable);
}
