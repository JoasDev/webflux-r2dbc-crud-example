package io.gitlab.joasdev.crud.example.configuration;

import java.io.File;
import java.util.Arrays;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.r2dbc.connection.init.ConnectionFactoryInitializer;
import org.springframework.r2dbc.connection.init.ResourceDatabasePopulator;
import org.springframework.web.reactive.config.CorsRegistry;
import org.springframework.web.reactive.config.EnableWebFlux;
import org.springframework.web.reactive.config.WebFluxConfigurer;

import io.r2dbc.spi.ConnectionFactory;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Configuration
//@EnableAutoConfiguration
@EnableWebFlux
//@EnableR2dbcRepositories
public class AppConfig implements WebFluxConfigurer{

    @Value("${app.cors.pathPattern:/**}")
    private String pathPattern;
    
    @Value("${app.cors.allowedOrigins:*}")
    private String[] allowedOrigins;
    
    @Value("${app.cors.allowedHeaders:*}")
    private String[] allowedHeaders;
    
    @Value("${app.cors.allowedMethods:*}")
    private String[] allowedMethods;
    
    @Value("${app.cors.maxAge:1800}")
    private long maxAge;
    
    @Override
    public void addCorsMappings(CorsRegistry corsRegistry) {
        
        log.info("pathPattern: {}",pathPattern);
        log.info("allowedOrigins: {}",Arrays.toString(allowedOrigins));
        log.info("allowedMethods: {}",Arrays.toString(allowedMethods));
        log.info("maxAge: {}",maxAge);
        
        corsRegistry.addMapping(pathPattern)
            .allowedHeaders(allowedHeaders)
            .allowedOrigins(allowedOrigins)
            .allowedMethods(allowedMethods)
            .maxAge(maxAge);
    }
    
    @Bean
    public ConnectionFactoryInitializer initializer(ConnectionFactory connectionFactory) {
      File schema = new File(System.getProperty("user.dir")+File.separator+"data");
      
      if(!schema.exists()) {
          schema.mkdir();
      }
      
      String[] contenido = schema.list();
      
      if(contenido.length > 0) {
          return null;
      }
        
      ConnectionFactoryInitializer initializer = new ConnectionFactoryInitializer();
      initializer.setConnectionFactory(connectionFactory);
      initializer.setDatabasePopulator(new ResourceDatabasePopulator(new ClassPathResource("schema.sql")));

      return initializer;
    }
    
    @Bean
    public ModelMapper getModelMapper() {
        return new ModelMapper();
    }
    
}
