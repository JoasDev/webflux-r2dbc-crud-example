package io.gitlab.joasdev.crud.example.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import io.gitlab.joasdev.crud.example.model.dto.BookCriteria;
import io.gitlab.joasdev.crud.example.model.dto.RegisterBookDto;
import io.gitlab.joasdev.crud.example.model.projection.BookProjection;
import reactor.core.publisher.Mono;

public interface BookService {

    Mono<Void> saveBook(RegisterBookDto registerBookDto);

    Mono<Page<BookProjection>> findAllToPage(BookCriteria bookSpec, Pageable pageable);

    Mono<BookProjection> findBookId(Integer bookId);
    
    Mono<Void> deleteBook(Integer bookId);

}
