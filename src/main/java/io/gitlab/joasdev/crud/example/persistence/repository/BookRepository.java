package io.gitlab.joasdev.crud.example.persistence.repository;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;

import io.gitlab.joasdev.crud.example.persistence.dao.BookAuthorDao;
import io.gitlab.joasdev.crud.example.persistence.entity.Book;


public interface BookRepository extends ReactiveCrudRepository<Book, Integer>, BookAuthorDao{
/*
	@Query("SELECT count(b.book_id) "
            + "FROM books b WHERE b.title like '%' || :q || '%' ")
	Mono<Integer> findCountByQ(@Param("q") String q);*/

	//Flux<BookProjection> findByQ(String q, Pageable pageable);

	/*@Query("SELECT ba.book_id as bookId, b.title as title, "
			+ "b.publication_date as publicationDate, "
			+ "b.online_availability as onlineAvailability, "
			+ "GROUP_CONCAT(a.first_name||' '||a.last_name SEPARATOR ', ') as concatAuthors "
            + "FROM book_authors ba "
            + "INNER JOIN books b ON ba.book_id = b.book_id "
            + "INNER JOIN authors a ON ba.author_id = a.author_id "
            + "WHERE b.title like '%' || :q || '%' GROUP BY b.book_id "
            + "ORDER BY b.book_id asc "
            + "LIMIT :#{#pageable.pageSize} OFFSET :#{#pageable.offset}")
	Flux<BookProjection> findByQ(@Param("q") String q, @Param("pageable") Pageable pageable);*/

}
