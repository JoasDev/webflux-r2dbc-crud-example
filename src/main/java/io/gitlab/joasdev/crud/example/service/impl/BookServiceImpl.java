package io.gitlab.joasdev.crud.example.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.gitlab.joasdev.crud.example.exception.ApiException;
import io.gitlab.joasdev.crud.example.model.dto.BookCriteria;
import io.gitlab.joasdev.crud.example.model.dto.RegisterBookDto;
import io.gitlab.joasdev.crud.example.model.projection.BookProjection;
import io.gitlab.joasdev.crud.example.persistence.entity.Book;
import io.gitlab.joasdev.crud.example.persistence.entity.BookAuthor;
import io.gitlab.joasdev.crud.example.persistence.repository.BookRepository;
import io.gitlab.joasdev.crud.example.service.BookService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

@Slf4j
@RequiredArgsConstructor
@Service
public class BookServiceImpl implements BookService {

	private final BookRepository bookRepository;
	private final ModelMapper modelMapper;

	@Override
	@Transactional
	public Mono<Void> saveBook(RegisterBookDto registerBookDto) throws ApiException {

		Book book = null;

		try {
			book = modelMapper.map(registerBookDto, Book.class);
		} catch (Exception e) {
			log.error(e.getMessage());
			return Mono.error(new ApiException("Error in Detail", HttpStatus.NOT_FOUND));
		}

		return bookRepository.save(book).flatMap(bookEntity -> {

			List<BookAuthor> bookAuthors = registerBookDto.getAuthors().stream().map(authorId -> {

				return BookAuthor.builder()
						.authorId(authorId)
						.bookId(bookEntity.getBookId())
						.build();

			}).collect(Collectors.toList());

			return bookRepository.saveAllBookAuthor(bookAuthors).collectList().then();
		});

	}

	@Override
	public Mono<Page<BookProjection>> findAllToPage(BookCriteria bookCriteria, Pageable pageable) {
		/*
		 * Page<BookProjection> books = null;
		 * 
		 * try { books = bookRepository.findAllToPage(bookSpec, pageable); }catch
		 * (Exception e) { log.error(e.getMessage()); throw new
		 * ApiException("Error in query", HttpStatus.NOT_FOUND); }
		 * 
		 * return books;
		 */
		
		return bookRepository.findAllToPage(bookCriteria,pageable)
        		.collectList()
        		.switchIfEmpty(Mono.error(new ApiException("Not result", HttpStatus.NO_CONTENT)))
        		.zipWith(bookRepository.findCountBookAuthorByCriteria(bookCriteria))
        		.map(result -> new PageImpl<>(result.getT1(), pageable, result.getT2()));
		
	}

	@Override
	public Mono<BookProjection> findBookId(Integer bookId) throws ApiException {

		return bookRepository.findByBookId(bookId);
	}

	@Override
	@Transactional
	public Mono<Void> deleteBook(Integer bookId) throws ApiException {
		return bookRepository.existBookAuthorByBookId(bookId)
		.flatMap(existBookAuthor -> {
			
			if(!existBookAuthor) {
				
				return bookRepository.deleteById(bookId);
			}
			
			return bookRepository.deleteBookAuthorByBookId(bookId)
					.then( bookRepository.deleteById(bookId));
					
		});
	}

}
