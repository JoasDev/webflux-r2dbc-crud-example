package io.gitlab.joasdev.crud.example.persistence.entity;

import java.io.Serializable;

import org.springframework.data.relational.core.mapping.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Table(name = "book_authors")
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class BookAuthor implements Serializable{

    private static final long serialVersionUID = -6636165613629960798L;
    
    //@Id
    //private BookAuthorId bookAuthorId;
    
    private Integer bookId;
    
    private Integer authorId;
    
    /*
     * This is only demonstrative, the @Transient annotation serves to ignore it from the entity mapping but through a set we can set our attributes
     * */
    /*@Transient
    private  Book book;
    
    @Transient 
    private Author author;*/
    
}
