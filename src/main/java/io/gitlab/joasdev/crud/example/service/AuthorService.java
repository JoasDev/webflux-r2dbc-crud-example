package io.gitlab.joasdev.crud.example.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import io.gitlab.joasdev.crud.example.model.dto.AuthorCriteria;
import io.gitlab.joasdev.crud.example.model.dto.AuthorFilter;
import io.gitlab.joasdev.crud.example.model.dto.RegisterAuthorDto;
import io.gitlab.joasdev.crud.example.model.dto.UpdateAuthorDto;
import io.gitlab.joasdev.crud.example.model.projection.AuthorProjection;
import io.gitlab.joasdev.crud.example.persistence.entity.Author;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface AuthorService {

    Mono<Integer> saveAuthor(RegisterAuthorDto registerAuthorDto);

    Mono<Page<AuthorProjection>> findAllToPage(AuthorFilter authorFilter, Pageable pageable);
    
    Mono<AuthorProjection> updateAuthor(Integer authorId,UpdateAuthorDto updateAuthorDto);
    
    Mono<Void> deleteAuthor(Integer authorId);

    Mono<AuthorProjection> findAuthorId(Integer authorId);

	Flux<Author> findAll(AuthorCriteria authorCriteria);
}
