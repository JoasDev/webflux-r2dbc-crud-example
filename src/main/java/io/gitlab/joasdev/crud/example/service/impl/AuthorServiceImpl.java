package io.gitlab.joasdev.crud.example.service.impl;

import org.modelmapper.MappingException;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.gitlab.joasdev.crud.example.exception.ApiException;
import io.gitlab.joasdev.crud.example.model.dto.AuthorCriteria;
import io.gitlab.joasdev.crud.example.model.dto.AuthorFilter;
import io.gitlab.joasdev.crud.example.model.dto.RegisterAuthorDto;
import io.gitlab.joasdev.crud.example.model.dto.UpdateAuthorDto;
import io.gitlab.joasdev.crud.example.model.projection.AuthorProjection;
import io.gitlab.joasdev.crud.example.persistence.entity.Author;
import io.gitlab.joasdev.crud.example.persistence.repository.AuthorRepository;
import io.gitlab.joasdev.crud.example.persistence.repository.BookRepository;
import io.gitlab.joasdev.crud.example.service.AuthorService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Slf4j
@Service
@RequiredArgsConstructor
public class AuthorServiceImpl implements AuthorService{

    private final AuthorRepository authorRepository;
    private final BookRepository bookRepository;
    private final ModelMapper modelMapper;
    
    @Override
    @Transactional
    public Mono<Integer> saveAuthor(RegisterAuthorDto registerAuthorDto) {

        return Mono.just(registerAuthorDto)
                .flatMap(dto -> {
                    try {
                        return Mono.just(modelMapper.map(dto, Author.class));
                    } catch (MappingException e) {
                        log.error(e.getMessage());
                        return Mono.error(new ApiException("Error al insertar datos", HttpStatus.NOT_FOUND));
                    }
                })
                .flatMap(authorEntity -> {
                    
                    return authorRepository.saveAuthor(authorEntity);
                    
                });
    }

    @Override
    public Mono<Page<AuthorProjection>> findAllToPage(AuthorFilter authorFilter, Pageable pageable) {
    	    	
        return authorRepository.findByQ(authorFilter.getQ(),pageable)
        		.collectList()
        		.switchIfEmpty(Mono.error(new ApiException("Not result", HttpStatus.NO_CONTENT)))
        		.zipWith(authorRepository.findCountByQ(authorFilter.getQ()))
        		.map(result -> new PageImpl<>(result.getT1(), pageable, result.getT2()));
    }

    @Override
    @Transactional
    public Mono<Void> deleteAuthor(Integer authorId) {
    	
    	return bookRepository.existBookAuthorByAuthorId(authorId)
    			.flatMap(existBookAuthor ->{
    				
    				if(existBookAuthor) {
    					return bookRepository.deleteBookAuthorByAuthorId(authorId);
    				}
    				
    				return bookRepository.deleteBookAuthorByAuthorId(authorId)
    						.then(authorRepository.deleteById(authorId));
    				
    			});    	
    }

    @Override
    public Mono<AuthorProjection> findAuthorId(Integer authorId) {

        return authorRepository.findByUserId(authorId)
                .switchIfEmpty(Mono.error(new ApiException("Not result in AuthorId: "+authorId, HttpStatus.NOT_FOUND)));
    }

    @Override
    @Transactional
    public Mono<AuthorProjection> updateAuthor(Integer authorId,UpdateAuthorDto updateAuthorDto) throws ApiException {       
        
        return Mono.just(updateAuthorDto)
                .flatMap(dto -> {
                    try {
                        Author authorEntity = modelMapper.map(updateAuthorDto, Author.class);
                        authorEntity.setAuthorId(authorId);
                         return Mono.just(authorEntity);
                         
                    } catch (MappingException e) {
                        log.error(e.getMessage());
                        return Mono.error(new ApiException("Error in Update", HttpStatus.NOT_FOUND));
                    }
                }).flatMap(authorEntity -> {
                    return  authorRepository.update(authorEntity);
                }).flatMap(id -> authorRepository.findByUserId(id));
    }

	@Override
	public Flux<Author> findAll(AuthorCriteria authorCriteria) {
		
		return authorRepository.findAuthorsByCriteria(authorCriteria);
	}

    
    
}
